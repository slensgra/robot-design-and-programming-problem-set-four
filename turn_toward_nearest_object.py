from turtlebot import Turtlebot, stop_handler, init_node, TurtlebotVelocity

import rospy
import math

RATE = 30

MIN_TURN_SPEED = 0.1
MAX_TURN_SPEED = 0.5

def turn_toward_nearest(turtlebot):
    rate = rospy.Rate(turtlebot.rate)

    while not rospy.is_shutdown():
        nearest_angle = min(turtlebot.laser_distances, key=lambda laser_distance: laser_distance.distance).angle

        turn_speed = min(max((nearest_angle / math.pi * 2), MIN_TURN_SPEED), MAX_TURN_SPEED)
        next_velocity = TurtlebotVelocity(linear=0, angular=turn_speed)

        if math.fabs(nearest_angle) < 0.05:
            next_velocity = TurtlebotVelocity(linear=0, angular=0)
            break

        turtlebot.publish_velocity(next_velocity)

        rate.sleep()

def main():
    init_node()
    rospy.on_shutdown(stop_handler)

    turtlebot = Turtlebot(RATE)
    turtlebot.wait_for_initial_sensor_info()

    turn_toward_nearest(turtlebot)
    turtlebot.stop()


if __name__ == "__main__":
    main()
