## does the stuff for the polygon motion
# problem 3
import numpy
import math

POLYGON_VERTICES = [
    (229, 69),
    (182, 157),
    (255, 227),
    (347, 182),
    (331, 84),
    (229, 69),
]

POLYGON_CENTER = (269, 143)

CONTACT_POINTS = [
    (207, 106),
    (249, 223),
    (347, 171),
    (292, 76),
]


def get_contact_normal(edge):
    edge_direction = edge[1] - edge[0]  
    return numpy.matrix([
        [-edge_direction[1]],
        [edge_direction[0]]
    ])


def print_contact_normal_matrix(vertices):
    rows = [] 
    
    for i in range(1, len(vertices)):
        j = i -1
        print(
            get_contact_normal(
                (
                    vertices[j], vertices[i]    
                )
            )
        )



def distance(p1, p2):
    return numpy.linalg.norm(p2 - p1)


def translate(vec):
    return lambda point: point + vec


def get_alpha_value(vertex):
    return math.atan2(vertex[1], vertex[0])


def get_alpha_for_vertices(vertices, center):
    translated_vertices = list(map(translate(-1*center), vertices))

    return [
        get_alpha_value(vertex) for vertex in translated_vertices
    ]


def get_point_jacobian(point, polygon_center, theta, additional_rotation):
    length_to_point = distance(polygon_center, point)

    return numpy.matrix([
        [1, 0, -1 * length_to_point * math.sin(theta + additional_rotation)],
        [0, 1, length_to_point * math.cos(theta + additional_rotation)],
    ])


def print_contact_point_jacobians(points, additional_rotations, polygon_center, config_theta):
    for point, additional_rotation in zip(points, additional_rotations):
        print(get_point_jacobian(point, polygon_center, config_theta, additional_rotation))


if __name__ == "__main__":
    vertices = list(map(numpy.array, POLYGON_VERTICES))
    contacts = list(map(numpy.array, CONTACT_POINTS))
    center = numpy.array(POLYGON_CENTER)
    configuration = numpy.array([POLYGON_CENTER[0], POLYGON_CENTER[1], 0])

    additional_rotations = get_alpha_for_vertices(contacts, center)
    print(additional_rotations)

    print_contact_normal_matrix(vertices)
    print_contact_point_jacobians(contacts, additional_rotations, center, configuration[2])
