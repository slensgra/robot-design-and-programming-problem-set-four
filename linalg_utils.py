import numpy
import math

def normalize(vec):
    norm = numpy.linalg.norm(vec)
    normalized = [component / norm for component in vec]
    return numpy.array(normalized)


def distance(a, b):
    return numpy.linalg.norm(b - a)


def rotation_matrix_2d(theta):
    return numpy.matrix([
        [math.cos(theta), -math.sin(theta)],
        [math.sin(theta), math.cos(theta)]
    ])


def rotate_2d(vector, theta):
    rotation_matrix = rotation_matrix_2d(theta)

    return rotation_matrix * vector

def is_infinite(number):
    return number == float("inf") or number == float("-inf")
