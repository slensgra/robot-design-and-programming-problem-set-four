import numpy
import math
import rospy

from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from sensor_msgs.msg import LaserScan
from collections import namedtuple
from tf.transformations import euler_from_quaternion

from linalg_utils import distance, normalize, rotate_2d


TurtlebotVelocity = namedtuple('TurtlebotVelocity', ['linear', 'angular'])
LaserDistance = namedtuple('LaserDistance', ['angle', 'distance'])


def adjust_velocity(current_velocity, distance_to_go, min_velocity, max_velocity, decay_after):
    if distance_to_go < decay_after:
        return TurtlebotVelocity(*(max(distance_to_go, min_velocity) * normalize(current_velocity)))

    return TurtlebotVelocity(*(normalize(current_velocity) * max_velocity))


def pen_position(position, radius, theta):
    return position[:2] + numpy.array(
        [
            radius * math.cos(theta),
            radius * math.sin(theta)
        ]
    )


def stop_handler():
    cmd_vel_publisher = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
    move_action = Twist()
    cmd_vel_publisher.publish(move_action)


def init_node():
    rospy.init_node("turtlebot_node_sel")


class Turtlebot(object):
    def __init__(self, rate):
        self.odom_subscriber = rospy.Subscriber('/odom', Odometry, self.update_pose_and_twist)
        self.vel_publisher = rospy.Publisher('/cmd_vel', Twist, queue_size=max(rate / 4, 1))
        self.scanner_subscriber = rospy.Subscriber('/scan', LaserScan, self.update_scanner_data)

        self.rate = rate 
        self.radius = 0.067

        self.position = None
        self.pose = None
        self.twist = None
        self.velocity = None
        self.laser_distances = None
        self.laser_distance_angle_increment = None

    @property
    def heading_unit_vector(self):
        return pen_position(self.position, 1.0, self.position[2])

    def update_scanner_data(self, msg):
        self.laser_distances = []

        angle_start = msg.angle_min
        increment = msg.angle_increment
        self.laser_distance_angle_increment = increment

        for index in range(len(msg.ranges)):
            self.laser_distances.append(
                LaserDistance(
                    angle=angle_start + (index * increment), 
                    distance=msg.ranges[index]
                )
            )

    def wait_for_initial_sensor_info(self):
        # waits for the odometer subscriber to populate this turtlebot's information
        rate = rospy.Rate(self.rate)

        while self.velocity is None or self.laser_distances is None and not rospy.is_shutdown():
            rate.sleep()

    def get_velocity_to_move_toward_point(self, point):
        toward_target = normalize(point - self.pen_position)
        return normalize(self.velocity_from_pen_direction_change(toward_target))

    @property
    def theta(self):
        return self.position[2]

    @property
    def theta_2pi(self):
        theta = self.theta

        if theta < 0:
            return theta + math.pi / 2

        return theta

    @property
    def pen_position(self):
        return pen_position(self.position, self.radius, self.theta)

    @property
    def pen_jacobian(self):
        return numpy.matrix([
            [1.0, 0.0, self.radius * -1.0 * math.sin(self.theta)], 
            [0.0, 1.0, self.radius * math.cos(self.theta)]
        ])

    @property
    def configuration_change(self):
        theta = self.theta
        configuration = numpy.array(
            [self.twist.linear.x * math.cos(theta), self.twist.linear.y * math.sin(theta), self.twist.angular.z]
        )

        configuration.shape = (3, 1)
        return configuration

    @property
    def pen_draw_direction(self):
        draw_direction = self.pen_jacobian * self.configuration_change
        draw_direction.shape = (1, 2)

        return draw_direction

    @property
    def vw_to_configuration_change(self):
        cos_theta = math.cos(self.theta)
        sin_theta = math.sin(self.theta)

        return numpy.matrix([
            [cos_theta, 0],
            [sin_theta, 0],
            [0, 1],
        ])

    def to_global_frame(self, point):
        rotated = point - self.position[:2]
        point.shape = (2, 1)
        rotated = rotate_2d(point, self.position[2])
        rotated = numpy.asarray(rotated)

        return numpy.array([rotated[0][0], rotated[1][0]]) + self.position[:2]

    def velocity_from_pen_direction_change(self, desired_pen_direction):
        wheel_velocity_to_pen_speed = self.pen_jacobian * self.vw_to_configuration_change

        desired_pen_direction.shape = (2, 1)

        return TurtlebotVelocity(
            *(
                wheel_velocity_to_pen_speed.I * desired_pen_direction
            )
        )

    def stop(self):
        self.publish_velocity(TurtlebotVelocity(0.0, 0.0))

    def _velocity_from_twist(self, twist):
        return TurtlebotVelocity(
            linear=twist.linear.x,
            angular=twist.angular.z
        )

    def update_pose_and_twist(self, message):
        self.twist = message.twist.twist

        self.velocity = self._velocity_from_twist(self.twist)
        self.pose = message.pose.pose

        self.position = (
            self.pose.position.x,
            self.pose.position.y,
            euler_from_quaternion(
                [
                    self.pose.orientation.x,
                    self.pose.orientation.y,
                    self.pose.orientation.z,
                    self.pose.orientation.w
                ]
            )[2]
        )

    def publish_velocity(self, velocity):
        twist_cmd = Twist()
        twist_cmd.linear.x = velocity.linear
        twist_cmd.angular.z = velocity.angular

        self.vel_publisher.publish(twist_cmd)
