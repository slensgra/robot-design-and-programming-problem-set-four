from turtlebot import Turtlebot, stop_handler, init_node, TurtlebotVelocity, LaserDistance, adjust_velocity
import linalg_utils
import rospy
import math
import numpy

RATE = 40

MIN_TURN_SPEED = 0.1
MAX_TURN_SPEED = 0.3
MAX_LINEAR_SPEED = 0.5
MIN_LINEAR_SPEED = 0.1
DISTANCE_TOLERANCE = 0.05

MAX_VELOCITY = 0.2
MIN_VELOCITY = 0.15

WALL_DISTANCE_TOLERANCE = 0.5
WALL_ANGLE_TOLERANCE = 0.05


def get_point_from_scan(turtlebot, scan_object):
    distance = scan_object.distance
    angle = scan_object.angle

    point_in_bot_frame = numpy.array(
        [distance * math.cos(angle), distance * math.sin(angle)]        
    )

    return point_in_bot_frame


def get_wall_direction_and_points(turtlebot):
    wall_distance = 1.0
    wall_at_angle = math.pi / 2

    valid_scans = list(
        filter(
            lambda distance: wall_distance - WALL_DISTANCE_TOLERANCE <= distance.distance <= wall_distance + WALL_DISTANCE_TOLERANCE,
            turtlebot.laser_distances
        )
    )

    if len(valid_scans) < 2:
        print "not enough valid laser data to move forward"
        return TurtlebotVelocity(linear=0, angular=0)

    leftmost_scan = min(
        valid_scans,
        key=lambda distance: math.fabs(distance.angle - wall_at_angle)
    )

    next_scan = None
    #print 'leftmost scan', leftmost_scan

    try:
        next_scan = min(
            filter(
                lambda distance: distance.angle > leftmost_scan.angle + turtlebot.laser_distance_angle_increment * 2,
                valid_scans,
            ),
            key=lambda distance: distance.angle
        )
    except:
        print "Couldn't find a forward angle"
        return TurtlebotVelocity(linear=0, angular=0)

    #print 'next scan', next_scan
    leftmost_point = turtlebot.to_global_frame(get_point_from_scan(turtlebot, leftmost_scan))
    next_point = turtlebot.to_global_frame(get_point_from_scan(turtlebot, next_scan))

    #print 'leftmost point', leftmost_point
    #print 'next point', next_point

    wall_direction = linalg_utils.normalize(next_point - leftmost_point)
    wall_normal = linalg_utils.normalize(numpy.array([-wall_direction[1], wall_direction[0]]))
    #print 'wall direction', wall_direction

    move_toward = next_point + (wall_distance * wall_normal)

    return wall_direction, next_point, move_toward


def get_next_velocity(turtlebot):

    return None

    next_velocity = linalg_utils.normalize(turtlebot.get_velocity_to_move_toward_point(move_toward)) * 0.5
    return TurtlebotVelocity(linear=next_velocity[0][0], angular=next_velocity[1][0])


def average_pt(pts):
    average_x = numpy.mean([pt[0] for pt in pts]) 
    average_y = numpy.mean([pt[1] for pt in pts])

    return numpy.array([average_x, average_y])


def get_average_wall_direction_and_move_toward(turtlebot):
    samples = 700
    wall_directions = []
    move_towards = []
    wall_points = []

    rate = rospy.Rate(turtlebot.rate)

    for _ in range(samples):
        try:
            wall_direction, next_pt, move_toward = get_wall_direction_and_points(turtlebot)
        except:
            rate.sleep()
            continue

        wall_directions.append(wall_direction)
        move_towards.append(move_toward)
        wall_points.append(next_pt)
        rate.sleep()

    return average_pt(wall_directions), average_pt(move_towards), average_pt(wall_points)


def construct_wall_sample_points(wall_direction, initial_point_on_wall, sample_distance, number_samples):
    wall_direction = linalg_utils.normalize(wall_direction)
    wall_normal = linalg_utils.normalize(numpy.array([-wall_direction[1], wall_direction[0]]))

    wall_samples = []

    for sample_number in range(number_samples):
        wall_samples.append(
            (initial_point_on_wall - ((sample_distance * float(sample_number)) * wall_direction)) + wall_normal
        )

    return wall_samples


def update_move_toward_point_idx(trajectory, current_position, move_toward_point_idx):
    move_toward_point = trajectory[move_toward_point_idx]
    new_idx = move_toward_point_idx

    if linalg_utils.distance(move_toward_point, current_position) <= DISTANCE_TOLERANCE:
        new_idx += 1
        new_idx = new_idx % len(trajectory)

    return new_idx


def get_next_direction(previous_point, move_toward_point, turtlebot):
    current_position = turtlebot.pen_position

    toward_target = linalg_utils.normalize(move_toward_point - current_position)

    draw_line_direction = linalg_utils.normalize(move_toward_point - numpy.array(previous_point))
    from_line_source = current_position - previous_point

    draw_component = from_line_source.dot(draw_line_direction)

    # this means that the robot is along the line, so we adjust to get it to move toward the line
    nearest_point = previous_point + ((draw_component + 0.1) * draw_line_direction)
    toward_line = linalg_utils.normalize(nearest_point - current_position)

    return toward_target + 0.1 * toward_line


def get_next_velocity(turtlebot, trajectory, move_toward_point_idx):
    current_position = turtlebot.pen_position

    move_toward_point_idx = update_move_toward_point_idx(
        trajectory,
        current_position,
        move_toward_point_idx
    )

    move_toward_point = trajectory[move_toward_point_idx]
    previous_point = trajectory[move_toward_point_idx - 1]

    next_direction = get_next_direction(
        previous_point,
        move_toward_point,
        turtlebot
    )

    next_velocity = adjust_velocity(
        turtlebot.velocity_from_pen_direction_change(next_direction),
        linalg_utils.distance(turtlebot.position[:2], numpy.array(move_toward_point)),
        MIN_VELOCITY,
        MAX_VELOCITY,
        0.1
    )

    return next_velocity, move_toward_point_idx


def follow_wall(turtlebot):
    rate = rospy.Rate(turtlebot.rate)
    print('getting stats on the wall...')
    wall_direction, move_toward_point, point_on_wall = get_average_wall_direction_and_move_toward(turtlebot)
    print('Done! Wall direction is: ', wall_direction)

    trajectory = construct_wall_sample_points(wall_direction, point_on_wall, 1.0, 10)
    print('Sampled trajectory is:', trajectory)
    move_toward_point_idx = 0

    while not rospy.is_shutdown():
        next_velocity, move_toward_point_idx = get_next_velocity(turtlebot, trajectory, move_toward_point_idx)
        turtlebot.publish_velocity(next_velocity)

        rate.sleep()


def main():
    init_node()
    rospy.on_shutdown(stop_handler)

    turtlebot = Turtlebot(RATE)
    turtlebot.wait_for_initial_sensor_info()

    follow_wall(turtlebot)
    turtlebot.stop()


if __name__ == "__main__":
    main()
