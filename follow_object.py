from turtlebot import Turtlebot, stop_handler, init_node, TurtlebotVelocity
import rospy
import math

RATE = 40

MIN_TURN_SPEED = 0.1
MAX_TURN_SPEED = 0.3
MAX_LINEAR_SPEED = 0.5
MIN_LINEAR_SPEED = 0.1


def within_cone(laser_distance):
    angle = laser_distance.angle
    right = (math.pi * 2) - (math.pi / 4)
    left = (math.pi / 4)

    return angle <= left or angle >= right


def nearest_laser_distance_within_cone(turtlebot):
    suitable_distances = filter(within_cone, turtlebot.laser_distances)
    return min(suitable_distances, key=lambda distance: distance.distance)


def get_next_velocity(turtlebot, nearest_laser_distance):
    angle = nearest_laser_distance.angle
    distance = nearest_laser_distance.distance

    if distance == float("inf"):
        return TurtlebotVelocity(linear=0, angular=0)

    if angle <= math.pi / 4:
        turn_direction = 1
    elif angle > (math.pi * 2) - (math.pi / 15) or angle < math.pi / 15:
        turn_direction = 0
    else:
        turn_direction = -1

    if distance < 0.5:
        linear_direction = -1
    elif 0.49 < distance < 0.51:
        linear_direction = 0
    else:
        linear_direction = 1

    if distance < 1:
        linear_direction = linear_direction * min(distance, MIN_LINEAR_SPEED)

    return TurtlebotVelocity(linear=MAX_LINEAR_SPEED * linear_direction, angular=MAX_TURN_SPEED * turn_direction)


def follow_object(turtlebot):
    rate = rospy.Rate(turtlebot.rate)

    while not rospy.is_shutdown():
        nearest_within_cone = nearest_laser_distance_within_cone(turtlebot)
        next_velocity = get_next_velocity(turtlebot, nearest_within_cone)
        turtlebot.publish_velocity(next_velocity)

        rate.sleep()


def main():
    init_node()
    rospy.on_shutdown(stop_handler)

    turtlebot = Turtlebot(RATE)
    turtlebot.wait_for_initial_sensor_info()

    follow_object(turtlebot)
    turtlebot.stop()


if __name__ == "__main__":
    main()
